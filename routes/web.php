<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', function () {
    return to_route('main_page.index');
});
Route::get('main-page', 'MainPageController@index')->name('main_page.index');
Route::get('analytics', 'AnalyticController@index')->name('analytics.index')->middleware('auth');

Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
    Route::get('get-post/{post}', 'AjaxController@getOne')->name('main_page.get_one');

    Route::group(['middleware' => 'auth'], function () {
        Route::controller('AjaxController')->group(function () {
            Route::post('/add-comment/post/{post}', 'addCommentPost')->name('add_comment_post');
            Route::post('/add-comment/comment/{comment}', 'addCommentComment')->name('add_comment_comment');
            Route::get('/add-like/post/{post}', 'addLikePost')->name('add_like_post');
            Route::get('/add-like/comment/{comment}', 'addLikeComment')->name('add_like_comment');
            Route::post('/add-money', 'addMoney')->name('add_money');
            Route::post('/buy-boosterpack/{boosterpack}', 'buyBoosterpack')->name('buy_boosterpack');
        });
        Route::controller('AnalyticController')->group(function () {
            Route::get('/get-logs', 'ajaxGetLogs')->name('get_logs');
            Route::get('/get-users-wallets', 'ajaxGetUsersWallets')->name('get_users_wallets');
            Route::get('/get-boosterpack-histories', 'ajaxGetBoosterpackHistories')->name('get_boosterpack_histories');

        });
        });
});

