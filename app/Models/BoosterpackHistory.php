<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BoosterpackHistory
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property float $boosterpack_price
 * @property integer $received_likes
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class BoosterpackHistory extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'boosterpack_price', 'received_likes'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
