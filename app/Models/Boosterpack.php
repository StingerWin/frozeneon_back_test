<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Boosterpack
 * @package App\Models
 *
 * @property int $id
 * @property float $price
 * @property float $bank
 * @property integer $us
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Boosterpack extends Model
{
    use HasFactory;

    protected $fillable = ['price', 'bank', 'us'];

    protected $casts = [
        'price' => 'float',
        'bank' => 'float',
        'us' => 'integer',
    ];
}
