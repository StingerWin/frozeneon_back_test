<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property string $action
 * @property float $amount
 * @property float $amount_balance
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Log extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'action', 'amount', 'amount_balance'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
