<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $avatarfull
 * @property integer $rights
 * @property integer $likes_balance
 * @property float $likes_total_refilled
 * @property float $wallet_balance
 * @property float $wallet_total_refilled
 * @property float $wallet_total_withdrawn
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const RIGHT_ADMIN = 1, RIGHT_USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatarfull',
        'rights',
        'likes_balance',
        'likes_total_refilled',
        'wallet_balance',
        'wallet_total_refilled',
        'wallet_total_withdrawn',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'rights' => 'integer',
        'likes_balance' => 'integer',
        'likes_total_refilled' => 'float',
        'wallet_balance' => 'float',
        'wallet_total_refilled' => 'float',
        'wallet_total_withdrawn' => 'float',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function isAdmin(): bool
    {
        return $this->rights === self::RIGHT_ADMIN;
    }
}
