<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property integer $price
 */
class Item extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'price'];
}
