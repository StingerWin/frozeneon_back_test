<?php

namespace App\Http\Controllers;

use App\Models\Boosterpack;
use App\Models\Post;
use App\Services\PostService;

class MainPageController extends Controller
{
    /**
     * @param PostService $postService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $posts = Post::all();
        $boosterpacks = Boosterpack::all();

        return view('main_page', compact('posts', 'boosterpacks'));
    }
}
