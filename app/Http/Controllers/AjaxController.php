<?php

namespace App\Http\Controllers;

use App\Exceptions\MessageException;
use App\Http\Resources\CommentResource;
use App\Http\Resources\PostResource;
use App\Models\Log;
use App\Models\Boosterpack;
use App\Models\BoosterpackHistory;
use App\Models\Comment;
use App\Models\Post;
use App\Services\LogService;
use App\Services\BoosterpackService;
use App\Services\ItemService;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AjaxController extends Controller
{
    public function getOne(Post $post, PostService $postService): Response
    {
        $post->load('user');
        $post->comments_tree = $postService->buildTree($post->comments->load('user'));

        return \response(['post' => new PostResource($post)], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function addCommentPost(Request $request, Post $post): Response
    {
        $validated = $request->validate([
            'comment' => 'required'
        ]);

        $comment = Comment::create([
            'user_id' => auth()->id(),
            'post_id' => $post->id,
            'text' => $validated['comment']
        ]);

        $comment['user'] = auth()->user();

        return \response(['comment' => new CommentResource($comment)], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return Response
     */
    public function addCommentComment(Request $request, Comment $comment): Response
    {
        $validated = $request->validate([
            'comment' => 'required'
        ]);

        $new_comment = Comment::create([
            'user_id' => auth()->id(),
            'post_id' => $comment->post->id,
            'parent_id' => $comment->id,
            'text' => $validated['comment']
        ]);

        $new_comment['user'] = auth()->user();

        return \response(['comment' => new CommentResource($new_comment)], Response::HTTP_OK);
    }

    /**
     * @param Post $post
     * @return Response
     */
    public function addLikePost(Post $post): Response
    {
        $auth_user = auth()->user();

        if ($auth_user->likes_balance) {
            $post->likes++;
            $post->save();

            $auth_user->likes_balance--;
            $auth_user->save();
        }

        return \response(['likes' => $post->likes], Response::HTTP_OK);
    }

    /**
     * @param Comment $comment
     * @return Response
     */
    public function addLikeComment(Comment $comment): Response
    {
        $auth_user = auth()->user();
        if ($auth_user->likes_balance) {
            $comment->likes++;
            $comment->save();

            $auth_user->likes_balance--;
            $auth_user->save();
        }

        return \response(['likes' => $comment->likes], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param LogService $logService
     * @return Response
     */
    public function addMoney(Request $request, LogService $logService): Response
    {
        $validated = $request->validate([
            'sum' => 'required|numeric'
        ]);

        $auth_user = auth()->user();

        $auth_user->update([
            'wallet_balance' => $auth_user->wallet_balance + $validated['sum'],
            'wallet_total_refilled' => $auth_user->wallet_total_refilled + $validated['sum'],
        ]);

        $data_log = $logService->generateDataCreate('Replenishment of the balance', $validated['sum'], $auth_user);
        Log::create($data_log);

        return \response(['user' => $auth_user], Response::HTTP_OK);
    }

    /**
     * @param Boosterpack $boosterpack
     * @param ItemService $itemService
     * @param BoosterpackService $boosterpackService
     * @param LogService $logService
     * @return Response
     * @throws MessageException
     */
    public function buyBoosterpack(Boosterpack $boosterpack,
                                   ItemService $itemService,
                                   BoosterpackService $boosterpackService,
                                   LogService $logService): Response
    {
        $auth_user = auth()->user();

        if ($auth_user->wallet_balance < $boosterpack->price)
            throw new MessageException('Not enough money to buy Boosterpack!', Response::HTTP_CONFLICT);

        $item = $itemService->getRandom($boosterpack);

        $new_bank = $boosterpackService->calculationBankNumber($boosterpack, $item);

        $boosterpack->update(['bank' => $new_bank]);

        $auth_user->update([
            'likes_balance' => $auth_user->likes_balance + $item->price,
            'likes_total_refilled' => $auth_user->likes_total_refilled + $item->price,
            'wallet_balance' => $auth_user->wallet_balance - $boosterpack->price,
            'wallet_total_withdrawn' => $auth_user->wallet_total_withdrawn + $boosterpack->price,
        ]);

        $data_log = $logService->generateDataCreate("Buying a Boosterpack ($boosterpack->price$)", $boosterpack->price, $auth_user);
        Log::create($data_log);

        $data_history = $boosterpackService->generateDataHistory($boosterpack->price, $item->price);
        BoosterpackHistory::create($data_history);

        return \response(['user' => $auth_user, 'likes' => $item->price], Response::HTTP_OK);
    }
}
