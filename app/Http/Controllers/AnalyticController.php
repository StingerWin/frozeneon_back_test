<?php

namespace App\Http\Controllers;

use App\Http\Resources\BoosterpackHistoryResource;
use App\Http\Resources\LogResource;
use App\Http\Resources\UserWalletsResource;
use App\Models\BoosterpackHistory;
use App\Models\Log;
use App\Models\User;
use Illuminate\Http\Response;

class AnalyticController extends Controller
{
    public function index()
    {
        $auth_admin = auth()->user();
        $auth_admin['is_admin'] = $auth_admin->isAdmin();

        return view('analytics', compact('auth_admin'));
    }

    public function ajaxGetLogs(): Response
    {
        $logs = Log::query();

        $auth_user = auth()->user();
        if (!$auth_user->isAdmin())
            $logs = $logs->where('user_id', $auth_user->id);

        $logs = $logs->where('created_at', '>', now()->subDays(30))
            ->orderBy('created_at', 'desc')->with('user')->get();

        return response(['logs' => LogResource::collection($logs)], Response::HTTP_OK);
    }

    public function ajaxGetUsersWallets(): Response
    {
        $users = User::query()->select('name', 'likes_total_refilled', 'wallet_total_refilled', 'wallet_total_withdrawn');

        $auth_user = auth()->user();
        if (!$auth_user->isAdmin())
            $users = $users->where('id', $auth_user->id);

        $users = $users->orderBy('created_at')->get();

        return response(['users' => UserWalletsResource::collection($users)], Response::HTTP_OK);
    }

    public function ajaxGetBoosterpackHistories(): Response
    {
        $histories = BoosterpackHistory::query();

        $auth_user = auth()->user();
        if (!$auth_user->isAdmin())
            $histories = $histories->where('user_id', $auth_user->id);

        $histories = $histories->where('created_at', '>', now()->subDays(30))
            ->orderBy('created_at', 'desc')->with('user')->get();

        return response(['histories' => BoosterpackHistoryResource::collection($histories)], Response::HTTP_OK);
    }
}
