<?php

namespace App\Http\Resources;

use App\Models\Log;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class LogResource
 * @package App\Http\Resources
 * @mixin Log
 */
class LogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'created_at' => $this->created_at->format('d.m.Y H:i')
        ]);
    }
}
