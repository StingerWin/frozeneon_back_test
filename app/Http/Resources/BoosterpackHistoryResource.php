<?php

namespace App\Http\Resources;

use App\Models\BoosterpackHistory;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BoosterpackHistoryResource
 * @package App\Http\Resources
 * @mixin BoosterpackHistory
 */
class BoosterpackHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
