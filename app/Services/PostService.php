<?php

namespace App\Services;

class PostService
{
    public function buildTree($items)
    {
        $grouped = $items->groupBy('parent_id');

        foreach ($items as $item) {
            if ($grouped->has($item->id)) {
                $item->children_tree = $grouped[$item->id]->values();
            }
        }

        return $items->where('parent_id', null)->values();
    }
}
