<?php


namespace App\Services;


use App\Models\User;

class LogService
{
    public function generateDataCreate(string $action, float $amount, User $user): array
    {
        return [
            'user_id' => $user->id,
            'action' => $action,
            'amount' => $amount,
            'amount_balance' => $user->wallet_balance,
        ];
    }
}
