<?php

namespace App\Services;

use App\Models\Boosterpack;
use App\Models\Item;

class BoosterpackService
{
    public function calculationBankNumber(Boosterpack $boosterpack, Item $item): float
    {
        return $boosterpack->bank + $boosterpack->price - $boosterpack->us - ($item->price ?? 0);
    }

    public function generateDataHistory(float $boosterpack_price, int $received_likes): array
    {
        return [
            'user_id' => auth()->id(),
            'boosterpack_price' => $boosterpack_price,
            'received_likes' => $received_likes
        ];
    }

}
