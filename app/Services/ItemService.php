<?php

namespace App\Services;

use App\Models\Boosterpack;
use App\Models\Item;

class ItemService
{
    public function getRandom(Boosterpack $boosterpack): Item|null
    {
        $max_item = $boosterpack->bank + $boosterpack->price - $boosterpack->us;

        return Item::query()->where('price', '<=', $max_item)->inRandomOrder()->first();
    }
}
