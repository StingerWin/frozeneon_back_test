<?php

namespace App\Exceptions;

use Exception;

class MessageException extends Exception
{
    public function render($request)
    {
        return response()->json([
            'message' => $this->getMessage()
        ], $this->getCode() ? $this->getCode() : 422);
    }
}
