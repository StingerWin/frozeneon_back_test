@extends('layouts.app')

@section('content')
    <div class="container mt-3">
        <div class="card">
            <div class="card-body">
                <analytics :auth-user-prop="{{ json_encode($auth_admin) }}"></analytics>
            </div>
        </div>
    </div>
@endsection
