@extends('layouts.app')

@section('content')
    @if($boosterpacks->isNotEmpty())
        <div class="posts">
            <h1 class="text-center">Posts</h1>
            <div class="container">
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-12 col-md-4 mt-2">
                            <post :post-prop="{{ json_encode($post) }}" :key="{{ $post->id }}"></post>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if($boosterpacks->isNotEmpty())
        <div class="boosterpacks">
            <h1 class="text-center">Boosterpack's</h1>
            <div class="container">
                <div class="row">
                    @foreach($boosterpacks as $boosterpack)
                        <div class="col-12 col-md-4 mt-2">
                            <boosterpack :boosterpack-prop="{{ json_encode($boosterpack) }}"
                                         :auth-user-prop="{{ json_encode(auth()->user()) }}">
                            </boosterpack>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endsection
