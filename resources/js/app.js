/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('header-user', require('./components/Header/HeaderUser.vue').default);
Vue.component('post', require('./components/Posts/Post.vue').default);
Vue.component('boosterpack', require('./components/Boosterpacks/Boosterpack.vue').default);
Vue.component('analytics', require('./components/Analytics/Analytics.vue').default);

Vue.component('post-popup', require('./components/Popups/PostPopup.vue').default);
Vue.component('post-comments', require('./components/Popups/Components/PostComments.vue').default);
Vue.component('login-popup', require('./components/Popups/LoginPopup.vue').default);
Vue.component('add-money-popup', require('./components/Popups/AddMoneyPopup.vue').default);
Vue.component('buy-boosterpack-popup', require('./components/Popups/BuyBoosterpackPopup.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Function from "./components/Function";

Vue.mixin(Function)

const app = new Vue({
    el: '#app',
});
