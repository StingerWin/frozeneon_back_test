<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique()->index();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('avatarfull')->nullable();
            $table->tinyInteger('rights');
            $table->integer('likes_balance')->default(0)->nullable();
            $table->decimal('likes_total_refilled', 10, 2)->default(0);
            $table->decimal('wallet_balance', 10, 2)->default(0);
            $table->decimal('wallet_total_refilled', 10, 2)->default(0);
            $table->decimal('wallet_total_withdrawn', 10, 2)->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
