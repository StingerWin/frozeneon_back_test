<?php

namespace Database\Seeders;

use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comments = [
            [
                'user_id' => 1,
                'post_id' => 1,
                'text' => 'Comment #1',
            ],
            [
                'user_id' => 1,
                'post_id' => 1,
                'text' => 'Comment #2',
            ]
        ];

        foreach ($comments as $comment) {
            Comment::create($comment);
        }
    }
}
