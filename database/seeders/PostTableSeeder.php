<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [
            [
                'user_id' => 1,
                'text' => 'Post 1',
                'img' => '/images/posts/1.png',
            ],
            [
                'user_id' => 1,
                'text' => 'Post 2',
                'img' => '/images/posts/2.png',
            ]
        ];

        foreach ($posts as $post) {
            Post::create($post);
        }
    }
}
