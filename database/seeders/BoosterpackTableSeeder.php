<?php

namespace Database\Seeders;

use App\Models\Boosterpack;
use Illuminate\Database\Seeder;

class BoosterpackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boosterpacks = [
            [
                'price' => 5,
                'us' => 1,
            ],
            [
                'price' => 20,
                'us' => 2,
            ],
            [
                'price' => 50,
                'us' => 5,
            ],
        ];

        foreach ($boosterpacks as $boosterpack) {
            Boosterpack::create($boosterpack);
        }
    }
}
