<?php

namespace Database\Seeders;

use App\Models\Item;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => '1 Likes',
                'price' => 1
            ],
            [
                'name' => '2 Likes',
                'price' => 2
            ],
            [
                'name' => '3 Likes',
                'price' => 3
            ],
            [
                'name' => '5 Likes',
                'price' => 5
            ],
            [
                'name' => '10 Likes',
                'price' => 10
            ],
            [
                'name' => '15 Likes',
                'price' => 15
            ],
            [
                'name' => '20 Likes',
                'price' => 20
            ],
            [
                'name' => '30 Likes',
                'price' => 30
            ],
            [
                'name' => '50 Likes',
                'price' => 50
            ],
            [
                'name' => '100 Likes',
                'price' => 100
            ],
            [
                'name' => '200 Likes',
                'price' => 200
            ],
            [
                'name' => '500 Likes',
                'price' => 500
            ],
        ];

        foreach ($items as $item) {
            Item::updateOrCreate(['price' => $item['price']], $item);
        }
    }
}
